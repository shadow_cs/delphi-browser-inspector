## BrowserInspector ##
Delphi's `TWebBrowser` component inspector (Dev tool).

## Description ##
The component allows you to inspect web browser elements and access JavaScript variables through simple console that resembles to Google Chrome Dev tools or Firebug.

## Usage ##
Just create `TfrmBrowserInspector` with `TWebBrowser` as owner in the constructor.

## Handling browser exceptions ##
Must be done manually, see https://support.microsoft.com/en-us/kb/261003 and http://stackoverflow.com/questions/24988467/how-can-i-confirm-script-error-dialog-box-into-webbrowser.

May be done by using and extending `TNulWBContainer` (https://github.com/jasonpenny/twebbrowser.utilities/blob/master/lib/Others/UNulContainer.pas).

## Troubleshooting ##
If the inspector is not working as expected, make sure to ass at least one `<script>` tag to your HTML, without it the `eval` method may not work.

## Thanks to ##
* Virtual-TreeView (https://github.com/Virtual-TreeView/Virtual-TreeView)

## License ##
Sources are licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0).

Copyright (c) 2016 Honza Rameš