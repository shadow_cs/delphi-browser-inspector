{***************************************************************************}
{                                                                           }
{           BrowserInspector for Delphi                                     }
{                                                                           }
{           Copyright (c) Honza Rames (ramejan@gmail.com)                   }
{                                                                           }
{           https://bitbucket.org/shadow_cs/delphi-browser-inspector        }
{                                                                           }
{***************************************************************************}
{                                                                           }
{  Licensed under the Apache License, Version 2.0 (the "License");          }
{  you may not use this file except in compliance with the License.         }
{  You may obtain a copy of the License at                                  }
{                                                                           }
{      http://www.apache.org/licenses/LICENSE-2.0                           }
{                                                                           }
{  Unless required by applicable law or agreed to in writing, software      }
{  distributed under the License is distributed on an "AS IS" BASIS,        }
{  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. }
{  See the License for the specific language governing permissions and      }
{  limitations under the License.                                           }
{                                                                           }
{***************************************************************************}

unit BrowserInspector;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, ComObj,
{$WARN UNIT_DEPRECATED OFF}
  Vcl.OleAuto,
{$WARN UNIT_DEPRECATED ON}
  StrUtils, Controls, Forms, Dialogs, StdCtrls, Generics.Collections, ShlObj,
  ComCtrls, ActnList, System.Actions, SHDocVw, MSHTML, ActiveX, Vcl.ToolWin,
  Vcl.ImgList, Vcl.AppEvnts, VirtualTrees, VirtualTreeWrapper, Vcl.StdActns,
  ClipBrd;

{$IF CompilerVersion >= 25} // >= XE4
	{$LEGACYIFEND ON}
{$IFEND}

type
  TNodeType = (ntCommand, ntText, ntElement, ntObject, ntArray, ntError, ntComment,
	ntCSSStyle, ntString, ntBoolean, ntNumber, ntNull);
  TNodeData = record
	Info: TVSTData;
	Prefix: string;
	Data: Variant;
	NodeType: TNodeType;
  end;

  TfrmBrowserInspector = class(TForm)
	txtCommand: TEdit;
	ToolBar: TToolBar;
	tbInspectElement: TToolButton;
	Images: TImageList;
	Actions: TActionList;
	InspectElement: TAction;
	ApplicationEvents: TApplicationEvents;
	vstTree: TVirtualStringTree;
	ToolButton1: TToolButton;
	procedure txtCommandKeyDown(Sender: TObject; var Key: Word;
	  Shift: TShiftState);
	procedure txtCommandChange(Sender: TObject);
	procedure InspectElementExecute(Sender: TObject);
	procedure tbInspectElementMouseDown(Sender: TObject; Button: TMouseButton;
	  Shift: TShiftState; X, Y: Integer);
	procedure ApplicationEventsIdle(Sender: TObject; var Done: Boolean);
	procedure vstTreeMeasureItem(Sender: TBaseVirtualTree;
	  TargetCanvas: TCanvas; Node: PVirtualNode; var NodeHeight: Integer);
	procedure vstTreeCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode;
	  Column: TColumnIndex; out EditLink: IVTEditLink);
	procedure vstTreeNodeDblClick(Sender: TBaseVirtualTree;
	  const HitInfo: THitInfo);
	procedure ToolButton1Click(Sender: TObject);
	procedure vstTreeInitChildren(Sender: TBaseVirtualTree; Node: PVirtualNode;
	  var ChildCount: Cardinal);
	procedure vstTreeKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
	procedure vstTreeDrawText(Sender: TBaseVirtualTree; TargetCanvas: TCanvas;
	  Node: PVirtualNode; Column: TColumnIndex; const AText: string;
	  const CellRect: TRect; var DefaultDraw: Boolean);
  public const
	HISTORY_FILE = 'BrowserInspector_History.txt';
  private
	FHistory: TStringList;
	FHistoryIndex: Integer;
	FHistoryFileName: string;
	Tree: TVirtualTreeWrapper<TNodeData>;
	function GetBrowser: TWebBrowser;
	procedure InspectJSObject(const Obj: IDispatchEx; const Prefix: string;
	  const Members: TStrings);
	function SerializeSimple(const Data: Variant; out NodeType: TNodeType): string;
	procedure SerializeNode(const Node: Variant; Parent: PVirtualNode);
	function SerializeElement(const Elem: Variant): string;
	function SerializeElementInner(const Elem: Variant; Parent: PVirtualNode): Integer;
{$IF Declared(IHTMLCSSStyleDeclaration)}
	function SerializeCSSStyleInner(const Style: Variant; Parent: PVirtualNode): Integer;
{$IFEND}
	function SerializeJSObject(const Obj: IDispatchEx; Parent: PVirtualNode): Integer;
	function SerializeVarArray(const Arr: Variant; Parent: PVirtualNode): Integer;
	procedure Serialize(const Data: Variant; const Prefix: string = '';
	  Parent: PVirtualNode = nil);
	procedure LoadSuggestions(const Cmd: string;
	  const Prefix: string);
	function Add(const Prefix, Text: string; NodeType: TNodeType;
	  Parent: PVirtualNode = nil): TVirtualNode<TNodeData>;
	procedure AddStructured(const Prefix, Text: string; NodeType: TNodeType;
	  const AData: Variant; Parent: PVirtualNode);
	procedure AddObject(const Prefix: string; const Obj: Variant; Parent: PVirtualNode);
	procedure AddElement(const Prefix: string; const Elem: Variant; Parent: PVirtualNode);
	procedure AddCSSStyle(const Prefix: string; const Style: Variant; Parent: PVirtualNode);
	procedure ScriptException(Sender: TObject; E: Exception);
	function IsError(const Dispatch: IDispatch): Boolean; overload;
	function IsError(const Data: Variant): Boolean; overload;
	procedure ResetHistory;
  protected
	FSuggestions: TStrings;
	FAutoComplete: IAutoComplete2;
	FScriptException: TClass;
	FScriptExceptionMsg: string;
	procedure Execute(Cmd: string);
	procedure Reset;
	property Browser: TWebBrowser read GetBrowser;
  public
	constructor Create(const Owner: TWebBrowser;
		const HistoryFileName: string = HISTORY_FILE); reintroduce;
	destructor Destroy; override;

	function Eval(const Cmd: string): Variant;
  end;

  TStringsEnumString = class(TInterfacedObject, IEnumString)
  private type
	TPtrArray = array[0..0] of Pointer;
  private
	FItems: TStrings;
	FCurrent: Integer;
	function GetCount: Integer; inline;
	function GetItem(Index: Integer): string;
  protected
	{ IEnumString }
	function Clone(out enm: IEnumString): HRESULT; stdcall;
	function Next(celt: Integer; out elt; pceltFetched: PLongint): HRESULT; stdcall;
	function Reset: HRESULT; stdcall;
	function Skip(celt: Integer): HRESULT; stdcall;

	property Count: Integer read GetCount;
	property Items[Index: Integer]: string read GetItem; default;
  public
	constructor Create(const Items: TStrings);
  end;

implementation

uses TypInfo;

{$R *.dfm}

const
	sIsError = '__is_error__';

type
	TWinControlAccess = class(TWinControl);
	TCOMActivator = record
		class procedure CreateInstance(const CLSID, GUID: TGUID; out Result); overload; static; inline;
		class function CreateInstance(const CLSID: TGUID): IUnknown; overload; static;
		class function CreateInstance<T: IInterface>(const CLSID: TGUID): T; overload; static;
	end;

	TMyStringEditLink = class(TStringEditLink)
	public
		function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; override;
	end;

class procedure TCOMActivator.CreateInstance(const CLSID, GUID: TGUID; out Result);
begin
	OleCheck(CoCreateInstance(CLSID, nil, CLSCTX_INPROC_SERVER, GUID, Result));
end;

class function TCOMActivator.CreateInstance(const CLSID: TGUID): IUnknown;
begin
	CreateInstance(CLSID, IUnknown, Result);
end;

class function TCOMActivator.CreateInstance<T>(const CLSID: TGUID): T;
begin
	Assert(PTypeInfo(TypeInfo(T)).Kind = tkInterface);
	CreateInstance(CLSID, PTypeInfo(TypeInfo(T)).TypeData.GUID, Result);
end;

{ TMyStringEditLink }

function TMyStringEditLink.PrepareEdit(Tree: TBaseVirtualTree;
	Node: PVirtualNode; Column: TColumnIndex): Boolean;
begin
	Result:=inherited;
	if (Result) then
		Edit.ReadOnly:=true;
end;

procedure TfrmBrowserInspector.txtCommandChange(Sender: TObject);
begin
	if (txtCommand.Text = '') then
		Reset;
end;

procedure TfrmBrowserInspector.txtCommandKeyDown(Sender: TObject; var Key: Word;
	Shift: TShiftState);
var	Text, s: string;
	DropDown: IAutoCompleteDropDown;
	Status: DWORD;
	First: PChar;
begin
	case Key of
		VK_RETURN: begin
			Key:=0;
			Execute(txtCommand.Text);
			Reset;
		end;
		VK_UP,
		VK_DOWN: begin
			//If suggestins are open, this is not even called (desired bahavior).
			if (Key = VK_UP) then
				Dec(FHistoryIndex)
			else Inc(FHistoryIndex);
			if (FHistoryIndex < 0) then
				FHistoryIndex:=0;
			if (FHistoryIndex >= FHistory.Count) then
				FHistoryIndex:=FHistory.Count - 1;
			if (FHistoryIndex < 0) then
				Exit;
			txtCommand.Text:=FHistory[FHistoryIndex];
			SendMessage(txtCommand.Handle, EM_SETSEL, MaxInt, MaxInt);
			Key:=0;
		end;
		VK_OEM_PERIOD: begin
			Text:=txtCommand.Text;
			if (Text.Trim = '') then
				Exit;
			if (Length(Text) = txtCommand.SelStart) then begin
				DropDown:=FAutoComplete as IAutoCompleteDropDown;
				if (Succeeded(DropDown.GetDropDownStatus(Status, First))) then
				begin
					if (Assigned(First)) then begin
						s:=First;
						CoTaskMemFree(First);
					end;
					if (Status and ACDD_VISIBLE <> 0) then begin
						if (s = '') then //None selected, find the first matching one ourselves
							for s in FSuggestions do
								if (s.StartsWith(Text, true)) then
						begin
							Text:=s;
							txtCommand.Text:=Text;
							SendMessage(txtCommand.Handle, EM_SETSEL, MaxInt, MaxInt);
						end;
						//else text is already in the edit
					end;
				end;
				LoadSuggestions(Text, Text + '.');
				DropDown.ResetEnumerator;
			end;
		end;
	end;
end;

procedure TfrmBrowserInspector.vstTreeDrawText(Sender: TBaseVirtualTree;
	TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
	const AText: string; const CellRect: TRect; var DefaultDraw: Boolean);
var R : TRect;
	OldStyle: TFontStyles;
	OldColor: TColor;
begin
	DefaultDraw:=false;
	with vstTree, Tree[Node].Data^, TargetCanvas do begin
		R:=CellRect;
		OldColor:=Font.Color;
		OldStyle:=Font.Style;
		case NodeType of
			ntNull,
			ntComment,
			ntCommand: Font.Color:=clGrayText;
			ntString: Font.Color:=clBlue;
			ntNumber: Font.Color:=$9D3E7A;
			ntBoolean: Font.Color:=$3137AA;
			ntElement: ;
			ntObject,
			ntArray,
			ntCSSStyle: Font.Style:=Font.Style + [fsBold];
			ntError: Font.Color:=clRed;
		end;
		Windows.DrawTextW(Handle, PChar(AText), Length(AText), R,
			DT_NOPREFIX or DT_WORDBREAK or DT_EDITCONTROL or DT_LEFT or DT_TOP);
		if ((OldStyle <> Font.Style) or (OldColor <> Font.Color)) then begin
			Font.Style:=OldStyle;
			Font.Color:=OldColor;
			if (Prefix <> '') then begin
				//Calculate prefix only rect
				Windows.DrawTextW(Handle, PChar(Prefix), Length(Prefix), R,
					DT_NOPREFIX or DT_WORDBREAK or DT_EDITCONTROL or DT_LEFT
					or DT_TOP or DT_CALCRECT);
				//Clear the rect to background color
				if (vsSelected in Node.States) then begin
					if (Focused or (toPopupMode in TreeOptions.PaintOptions)) then
						Brush.Color:=Colors.FocusedSelectionColor
					else Brush.Color:=Colors.UnfocusedSelectionColor;
				end
				else Brush.Color:=vstTree.Color;
				Brush.Style:=bsSolid;
				FillRect(R);
				//Draw the prefix with original color
				Windows.DrawTextW(Handle, PChar(Prefix), Length(Prefix), R,
					DT_NOPREFIX or DT_WORDBREAK or DT_EDITCONTROL or DT_LEFT
					or DT_TOP);
			end;
		end;
	end;
end;

procedure TfrmBrowserInspector.vstTreeCreateEditor(Sender: TBaseVirtualTree;
	Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
begin
	EditLink:=TMyStringEditLink.Create;
end;

procedure TfrmBrowserInspector.vstTreeInitChildren(Sender: TBaseVirtualTree;
	Node: PVirtualNode; var ChildCount: Cardinal);
begin
	with Tree[Node].Data^ do begin
		case NodeType of
			ntObject, ntArray, ntError: begin
				if (VarIsArray(Data)) then
					ChildCount:=SerializeVarArray(Data, Node)
				else ChildCount:=SerializeJSObject(IDispatch(Data) as IDispatchEx, Node);
			end;
			ntElement: ChildCount:=SerializeElementInner(Data, Node);
{$IF Declared(IHTMLCSSStyleDeclaration)}
			ntCSSStyle: ChildCount:=SerializeCSSStyleInner(Data, Node);
{$IFEND}
		end;
	end;
end;

procedure TfrmBrowserInspector.vstTreeKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const LEVELS = '                                                                                ';
var	Node: PVirtualNode;
	s: string;
begin
	if ((Key = Ord('C')) and (ssCtrl in Shift)) then begin
		Node:=vstTree.GetFirstSelected;
		while (Assigned(Node)) do begin
			with Tree[Node] do
				s:=s + Copy(LEVELS, 1, Level) + Caption + sLineBreak;
			Node:=vstTree.GetNextSelected(Node);
		end;
		if (s <> '') then
			Clipboard.AsText:=s.TrimRight;
	end;
end;

procedure TfrmBrowserInspector.vstTreeMeasureItem(Sender: TBaseVirtualTree;
	TargetCanvas: TCanvas; Node: PVirtualNode; var NodeHeight: Integer);
begin
	TargetCanvas.Font:=Sender.Font;
	NodeHeight:=TCustomVirtualStringTree(Sender).ComputeNodeHeight(TargetCanvas,
		Node, 0) + 4;
end;

procedure TfrmBrowserInspector.vstTreeNodeDblClick(Sender: TBaseVirtualTree;
	const HitInfo: THitInfo);
begin
	if (not Assigned(HitInfo.HitNode)) then
		Exit;

	with Tree[HitInfo.HitNode] do begin
		case Data.NodeType of
			ntCommand: txtCommand.Text:=Copy(Caption, 1, Caption.Length - 1);
		end;
	end;
	vstTree.EditNode(HitInfo.HitNode, HitInfo.HitColumn);
end;

function TfrmBrowserInspector.Add(const Prefix, Text: string; NodeType: TNodeType;
	Parent: PVirtualNode = nil): TVirtualNode<TNodeData>;
begin
	Result:=Tree[Tree.AddChild(Parent, Prefix + Text)];
	Result.MultiLine:=true;
	Result.Data.NodeType:=NodeType;
	Result.Data.Prefix:=Prefix;
	if (Parent = nil) then
		vstTree.ScrollIntoView(Result, false);
end;

procedure TfrmBrowserInspector.AddCSSStyle(const Prefix: string;
  const Style: Variant; Parent: PVirtualNode);
begin
	AddStructured(Prefix, '(style)', ntCSSStyle, Style, Parent);
end;

procedure TfrmBrowserInspector.AddElement(const Prefix: string;
	const Elem: Variant; Parent: PVirtualNode);
begin
	AddStructured(Prefix, SerializeElement(Elem), ntElement, Elem, Parent);
end;

procedure TfrmBrowserInspector.AddObject(const Prefix: string;
	const Obj: Variant; Parent: PVirtualNode);
var	s: string;
	NodeType: TNodeType;
begin
	s:=SerializeSimple(Obj, NodeType);
	Assert(NodeType in [ntObject, ntArray, ntError]);
	AddStructured(Prefix, s, NodeType, Obj, Parent);
end;

procedure TfrmBrowserInspector.AddStructured(const Prefix, Text: string;
	NodeType: TNodeType; const AData: Variant; Parent: PVirtualNode);
begin
	with Add(Prefix, Text, NodeType, Parent) do begin
		HasChildren:=true;
		Data.Data:=AData;
	end
end;

procedure TfrmBrowserInspector.ApplicationEventsIdle(Sender: TObject;
	var Done: Boolean);
begin
	if (InspectElement.Checked and (GetCaptureControl <> tbInspectElement)) then
		InspectElement.Checked:=false;
end;

constructor TfrmBrowserInspector.Create(const Owner: TWebBrowser;
	const HistoryFileName: string);
begin
	inherited Create(Owner);
	Tree:=TVirtualTreeWrapper<TNodeData>.Create(vstTree);
	FSuggestions:=TStringList.Create;
	FHistoryFileName:=HistoryFileName;
	FHistory:=TStringList.Create;
	if (FileExists(FHistoryFileName)) then begin
		FHistory.LoadFromFile(FHistoryFileName, TEncoding.UTF8);
		ResetHistory;
	end;
	FAutoComplete:=TCOMActivator.CreateInstance<IAutoComplete2>(CLSID_AutoComplete);
	FAutoComplete.Init(txtCommand.Handle, TStringsEnumString.Create(FSuggestions),
		nil, nil);
	FAutoComplete.SetOptions(ACO_AUTOSUGGEST or ACO_FILTERPREFIXES
		//or ACO_USETAB
		//or ACO_UPDOWNKEYDROPSLIST
		//or ACO_NOPREFIXFILTERING
		);
	//Inject script that allows us to determine object name (does not work in
	//for embedded objects like window or document in MSIE :( )
	(*Eval(
		'Object.prototype.__getName = function() {' +
			'var funcNameRegex = /function (.{1,})\(/;' +
			'var results = (funcNameRegex).exec((this).constructor.toString());'+
			'return (results && results.length > 1) ? results[1] : "";'+
		'};');*)
	Reset;
end;

destructor TfrmBrowserInspector.Destroy;
begin
	FAutoComplete:=nil;
	if (Assigned(FHistory)) then try
		FHistory.SaveToFile(FHistoryFileName, TEncoding.UTF8);
	except
	end;
	FHistory.Free;
	FSuggestions.Free;
	inherited;
end;

function TfrmBrowserInspector.GetBrowser: TWebBrowser;
begin
	Result:=TWebBrowser(Owner);
end;

function TfrmBrowserInspector.SerializeSimple(const Data: Variant;
	out NodeType: TNodeType): string;
//**********
function IsArray(const Obj: Variant): Boolean;
var Dispatch: IDispatch;
	DispatchEx: IDispatchEx;
	ID, PrevID: TDispID;
	Name: TBStr;
	SName: string;
	//LengthFound: Boolean;
	Count, i: Integer;
begin
	Result:=false;
	Dispatch:=Data;
	if (not Supports(Dispatch, IDispatchEx, DispatchEx)) then
		Exit;
	//LengthFound:=false;
	Count:=0;
	ID:=DISPID_STARTENUM;
	PrevID:=DISPID_STARTENUM;
	while (DispatchEx.GetNextDispID(fdexEnumAll, ID, ID) = S_OK) do begin
		if (ID = DISPID_STARTENUM) or (ID = PrevID) then
			Break;
		PrevID:=ID;
		if (Succeeded(DispatchEx.GetMemberName(ID, Name))) then begin
			SName:=Name;
			SysFreeString(Name);
			if (SName = 'length') then begin
				//LengthFound:=true;
				Inc(Count);
			end
			else if (TryStrToInt(SName, i)) then
				Inc(Count)
			else Exit;
		end;
	end;
	Result:=(Count > 0) {and LengthFound};
end;
//**********
begin
	NodeType:=ntText;
	case TVarData(Data).VType of
		varEmpty: begin
			Result:='(empty)';
			NodeType:=ntNull;
		end;
		varNull: begin
			Result:='(null)';
			NodeType:=ntNull;
		end;
		varDispatch: begin
			if (IsError(Data)) then begin
				Result:='(' + Data.name + ': ' + Data.message + ')';
				NodeType:=ntError;
			end
			else if (IsArray(Data)) then begin
				Result:='(array)';
				NodeType:=ntArray;
			end
			else begin
				Result:='(object)';
				NodeType:=ntObject;
			end;
		end;
		varUnknown: Result:='(unknown)';
		else if VarIsArray(Data) then begin
			Result:='(array)';
			NodeType:=ntArray;
		end
		else begin
			Result:=Data;
			if (VarIsStr(Data)) then begin
				Result:='"' + Result + '"';
				NodeType:=ntString;
			end
			else if (VarIsType(Data, varBoolean)) then
				NodeType:=ntBoolean
			else if (VarIsNumeric(Data)) then
				NodeType:=ntNumber;
		end;
	end;
end;

function TfrmBrowserInspector.SerializeVarArray(const Arr: Variant;
  Parent: PVirtualNode): Integer;
var i: Integer;
begin
	Result:=0;
	Assert(VarArrayDimCount(Arr) = 1);
	Result:=VarArrayHighBound(Arr, 1) - VarArrayLowBound(Arr, 1) + 1;
	for i:=VarArrayLowBound(Arr, 1) to VarArrayHighBound(Arr, 1) do
		Serialize(Arr[i], IntTostr(i) + ': ', Parent);
end;

procedure TfrmBrowserInspector.tbInspectElementMouseDown(Sender: TObject;
	Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var	P: TPoint;
	V: Variant;
begin
	if (InspectElement.Checked) then begin
		P:=Point(X, Y);
		if (ClientRect.Contains(P)) then begin
			InspectElement.Execute;
			Exit;
		end;
		P:=tbInspectElement.ClientToScreen(P);
		P:=Browser.ScreenToClient(P);
		if (not Browser.ClientRect.Contains(P)) then begin
			InspectElement.Execute;
			Exit;
		end;
		V:=Browser.Document as IDispatch;
		V:=V.elementFromPoint(P.X, P.Y);
		Serialize(V)
	end;
end;

procedure TfrmBrowserInspector.ToolButton1Click(Sender: TObject);
begin
	vstTree.Clear;
end;

procedure TfrmBrowserInspector.InspectElementExecute(Sender: TObject);
begin
	if not InspectElement.Checked then begin
		InspectElement.Checked:=true;
		SetCaptureControl(tbInspectElement)
	end
	else begin
		SetCaptureControl(nil);
		InspectElement.Checked:=false;
	end;
end;

procedure TfrmBrowserInspector.InspectJSObject(const Obj: IDispatchEx; const Prefix: string;
	const Members: TStrings);
var	ID, PrevID: TDispID;
	Name: TBStr;
	SName: string;
begin
	ID:=DISPID_STARTENUM;
	PrevID:=DISPID_STARTENUM;
	while (Obj.GetNextDispID(fdexEnumAll, ID, ID) = S_OK) do begin
		if (ID = DISPID_STARTENUM) or (ID = PrevID) then
			Break;
		PrevID:=ID;
		if (Succeeded(Obj.GetMemberName(ID, Name))) then begin
			SName:=Name;
			SysFreeString(Name);
			if (SName <> sIsError) then
				Members.Add(Prefix + SName);
		end;
	end;
end;

function TfrmBrowserInspector.IsError(const Dispatch: IDispatch): Boolean;
var Names: array[0..0] of PChar;
	IDs: array[0..0] of TDispID;
begin
	Names[0]:=sIsError;
	Result:=Assigned(Dispatch) and Succeeded(Dispatch.GetIDsOfNames(GUID_NULL,
		@Names, 1, GetThreadLocale, @IDs));
end;

function TfrmBrowserInspector.IsError(const Data: Variant): Boolean;
begin
	Result:=VarIsType(Data, varDispatch) and IsError(IDispatch(Data));
end;

procedure TfrmBrowserInspector.LoadSuggestions(const Cmd: string;
	const Prefix: string);
var	Data: Variant;
	DispatchEx: IDispatchEx;
begin
	FSuggestions.Clear;
	Data:=Eval(Cmd);
	if (IsError(Data)) then
		Exit;
	if (VarIsType(Data, varDispatch)) then begin
		if (Supports(IDispatch(Data), IDispatchEx, DispatchEx)) then
			InspectJSObject(DispatchEx, Prefix, FSuggestions);
	end;
	if (SameText(Prefix, 'document.')) then begin
		FSuggestions.Add(Prefix + 'getElementById');
		FSuggestions.Add(Prefix + 'getElementsByTagName');
	end;
end;

procedure TfrmBrowserInspector.Reset;
begin
	txtCommand.Text:='';
	ResetHistory;
	try
		LoadSuggestions('window', '');
	except
		//Consume
	end;
end;

procedure TfrmBrowserInspector.ResetHistory;
begin
	FHistoryIndex:=FHistory.Count;
end;

procedure TfrmBrowserInspector.ScriptException(Sender: TObject; E: Exception);
begin
	FScriptException:=E.ClassType;
	FScriptExceptionMsg:=E.Message;
end;

procedure TfrmBrowserInspector.Serialize(const Data: Variant; const Prefix: string = '';
	Parent: PVirtualNode = nil);
var	Dispatch: IDispatch;
	NodeType: TNodeType;
	s: string;
begin
	if (VarIsType(Data, varDispatch)) then begin
		Dispatch:=Data;
		if (IsError(Dispatch)) then
			AddObject(Prefix, Data, Parent)
		else if (Supports(Dispatch, IHTMLElement2)) then
			AddElement(Prefix, Data, Parent)
		{else if Supports(Dispatch, IHTMLCurrentStyle) then
			AddCurrentStyle(Prefix, Data, Parent)}
{$IF Declared(IHTMLCSSStyleDeclaration)}
		else if Supports(Dispatch, IHTMLCSSStyleDeclaration) then
			AddCSSStyle(Prefix, Data, Parent)
{$IFEND}
		else if (Supports(Dispatch, IDispatchEx)) then
			AddObject(Prefix, Data, Parent)
		else begin
			s:=SerializeSimple(Data, NodeType);
			Add(Prefix, s, NodeType, Parent);
		end;
	end
	else if (VarIsArray(Data)) then begin
		s:=SerializeSimple(Data, NodeType);
		Assert(NodeType in [ntArray]);
		if (VarArrayDimCount(Data) = 1) then
			AddStructured(Prefix, s, NodeType, Data, Parent)
		else Add(Prefix, s, NodeType, Parent);
	end
	else begin
		s:=SerializeSimple(Data, NodeType);
		Add(Prefix, s, NodeType, Parent);
	end;
end;

{$IF Declared(IHTMLCSSStyleDeclaration)}
function TfrmBrowserInspector.SerializeCSSStyleInner(const Style: Variant;
  Parent: PVirtualNode): Integer;
var	i: Integer;
	LStyle: IHTMLCSSStyleDeclaration;
	Name: string;
begin
	Result:=1;
	AddObject('', Style, Parent);
	LStyle:=IDispatch(Style) as IHTMLCSSStyleDeclaration;
	for i:=0 to LStyle.length - 1 do begin
		Name:=LStyle.item(i);
		Serialize(LStyle.getPropertyValue(Name), Name + ': ', Parent);
		Inc(Result);
	end;
end;
{$IFEND}

function TfrmBrowserInspector.SerializeElement(const Elem: Variant): string;
var Inner, Outer: string;
	i: Integer;
begin
	Outer:=Elem.outerHTML;
	if Elem.children.length = 0 then
		Result:=Outer.Trim
	else begin
		Inner:=Elem.innerHTML;
		i:=Outer.LastIndexOf('</') + 1;
		if (i > 0) then
			i:=Outer.Length - i + 1;
		Result:=Copy(Outer, 1, outer.Length - inner.Length - i).Trim;
	end;
end;

function TfrmBrowserInspector.SerializeElementInner(const Elem: Variant;
	Parent: PVirtualNode): Integer;
var	i: Integer;
begin
	Result:=1;
	AddObject('', Elem, Parent);
	for i:=0 to Elem.childNodes.length - 1 do begin
		SerializeNode(Elem.childNodes[i], Parent);
		Inc(Result);
	end;
end;

function TfrmBrowserInspector.SerializeJSObject(const Obj: IDispatchEx;
	Parent: PVirtualNode): Integer;
const
	grfdexPropCanAll = (fdexPropCanGet or fdexPropCanPut or fdexPropCanPutRef or fdexPropCanCall or fdexPropCanConstruct or fdexPropCanSourceEvents);
	grfdexPropCannotAll = (fdexPropCannotGet or fdexPropCannotPut or fdexPropCannotPutRef or fdexPropCannotCall or fdexPropCannotConstruct or fdexPropCannotSourceEvents);
	grfdexPropExtraAll = (fdexPropNoSideEffects or fdexPropDynamicType);
	grfdexPropAll = (grfdexPropCanAll or grfdexPropCannotAll or grfdexPropExtraAll);
var	ID, PrevID: TDispID;
	Name: TBStr;
	//Props: DWORD;
	Data: OleVariant;
	HRes: HRESULT;
	Params: TDispParams;
	SName: string;
begin
	Result:=0;
	ID:=DISPID_STARTENUM;
	PrevID:=DISPID_STARTENUM;
	while (Obj.GetNextDispID(fdexEnumAll, ID, ID) = S_OK) do begin
		if (ID = DISPID_STARTENUM) or (ID = PrevID) then
			Break;
		PrevID:=ID;
		if ({Succeeded(Obj.GetMemberProperties(ID, grfdexPropAll, Props))
			and} Succeeded(Obj.GetMemberName(ID, Name))) then
		begin
			SName:=Name;
			SysFreeString(Name);
			if (SName = sIsError) then
				Continue;
			{if (Props and fdexPropCanGet <> 0) then }begin
				{HRes:=Obj.InvokeEx(ID, GetThreadLocale, DISPATCH_PROPERTYGET, nil,
					Data, EI, nil);}
				Params:=Default(TDispParams);
				HRes:=Obj.Invoke(ID, GUID_NULL, GetThreadLocale, DISPATCH_PROPERTYGET,
					Params, @Data, nil, nil);
				if (Succeeded(HRes)) then begin
					Serialize(Data, SName + ': ', Parent);
					Inc(Result);
				end;
			end;
		end;
	end;
end;

procedure TfrmBrowserInspector.SerializeNode(const Node: Variant; Parent: PVirtualNode);
const
	ELEMENT_NODE 	= 1;
	COMMENT_NODE	= 8;
begin
	case Node.nodeType of
		ELEMENT_NODE: AddElement('', Node, Parent);
		COMMENT_NODE: Add('', '<!--' + Node.nodeValue + '-->', ntComment, Parent);
		else Add('', Node.nodeValue, ntText, Parent);
	end;
end;

function TfrmBrowserInspector.Eval(const Cmd: string): Variant;
const
	sCommand =
		'(function() {' +
			'try {' +
				'return %s;' +
			'}' +
			'catch(e) {' +
				'e.' + sIsError + ' = true;' +
				'return e;' +
			'}' +
		'})()';
	sException =
		'(function(){'+
			'return {' +
				sIsError + ': true,' +
				'name: "%s",' +
				'message: "%s"' +
			'}' +
		'})()';
var	Document: IHTMLDocument2;
	Window: IHTMLWindow2;
	OldHandler: TExceptionEvent;
begin
	Result:=Default(Variant);
	Document:=Browser.Document as IHTMLDocument2;
	if not Assigned(Document) then
		Exit;
	Window:=Document.parentWindow;
	if not Assigned(Window) then
		Exit;
	Result:=Window as IDispatch;
	FScriptException:=nil;
	OldHandler:=Application.OnException;
	Application.OnException:=ScriptException;
	try try
		Result:=Result.eval(Format(sCommand, [Cmd]));
	except
		on E: Exception do begin
			if ((E is ComObj.EOleError) and Assigned(FScriptException)) then
				Result:=Result.eval(Format(sException, [FScriptException.ClassName,
					FScriptExceptionMsg]))
			else Result:=Result.eval(Format(sException, [E.ClassName, E.Message]));
		end;
	end;
	finally
		Application.OnException:=OldHandler;
	end;
end;

procedure TfrmBrowserInspector.Execute(Cmd: string);
var	i: Integer;
begin
	Cmd:=Cmd.Trim;
	i:=FHistory.IndexOf(Cmd);
	if (i >= 0) then
		FHistory.Delete(i);
	FHistory.Add(Cmd);
	Add('', Cmd + ':', ntCommand);
	Serialize(Eval(Cmd))
end;

{ TStringsEnumString }

function TStringsEnumString.Clone(out enm: IEnumString): HRESULT;
var
	NewEnum: TStringsEnumString;
begin
	NewEnum:=TStringsEnumString.Create(FItems);
	enm:=NewEnum;
	NewEnum.FCurrent:=FCurrent;
	Result:=S_OK;
end;

constructor TStringsEnumString.Create(const Items: TStrings);
begin
	inherited Create;
	FItems:=Items;
end;

function TStringsEnumString.GetCount: Integer;
begin
	Result:=FItems.Count;
end;

function TStringsEnumString.GetItem(Index: Integer): string;
begin
	Result:=FItems[Index];
end;

function TStringsEnumString.Next(celt: Integer; out elt;
	pceltFetched: PLongint): HRESULT;
var	i: Integer;
	s: UnicodeString;
	Size: Integer;
	LCount: Integer;
	P: PChar absolute s; //Use to prevent UniqueString call
begin
	i:=0;
	LCount:=Count;
	while ((i < celt) and (FCurrent < LCount)) do begin
		s:=Items[FCurrent];
		Size:=(s.Length + 1) * sizeof(WideChar);
		TPtrArray(elt)[i]:=CoTaskMemAlloc(Size);
		Move(P^, TPtrArray(elt)[i]^, Size);
		Inc(i);
		Inc(FCurrent);
	end;
	if (Assigned(pceltFetched)) then
		pceltFetched^:=i;
	if (i = celt) then
		Result:=S_OK
	else Result:=S_FALSE;
end;

function TStringsEnumString.Reset: HRESULT;
begin
	FCurrent:=0;
	Result:=S_OK;
end;

function TStringsEnumString.Skip(celt: Longint): HRESULT;
var	LCount: Integer;
begin
	LCount:=Count;
	FCurrent:=FCurrent + celt;
	if (FCurrent < LCount) then
		Result:=S_OK
	else begin
		FCurrent:=0;
		Result:=S_FALSE;
	end;
end;

end.

